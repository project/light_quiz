## requires 
* angularjs >= 1.3.4
* angular-route & angular-sanitize modules
* [angular-timer](http://siddii.github.io/angular-timer/) module
* [humanize-duration](https://github.com/EvanHahn/HumanizeDuration.js) library
* [moment](http://momentjs.com) library