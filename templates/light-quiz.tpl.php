<div id="light-quiz-app">
  <div ng-view></div>
  
  <?php print theme('light_quiz_test') ?>
  <?php print theme('light_quiz_question') ?>
  <?php print theme('light_quiz_result') ?>
  <?php print theme('light_quiz_feedback') ?>
</div>
